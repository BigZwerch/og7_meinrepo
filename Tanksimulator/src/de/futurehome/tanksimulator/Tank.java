package de.futurehome.tanksimulator;
public class Tank {
	
	private double fuellstand;
	private String erroar;

	public Tank(double fuellstand) {
		this.fuellstand = fuellstand;
		this.erroar = erroar;
	}

	public double getFuellstand() {
		return fuellstand;
	}

	public void setFuellstand(double fuellstand) {
		this.fuellstand = fuellstand;
	}
	
	public String getError() {
		return erroar;
	}

	public void setError(String error) {
		this.erroar = erroar;
	}
}
