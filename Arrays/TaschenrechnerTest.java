package taschenrechner;

import java.util.Scanner;

public class TaschenrechnerTest {

	public static void main(String[] args) {

		Scanner myScanner = new Scanner(System.in);
		Taschenrechner ts = new Taschenrechner();

		int swValue;
		int zahl1;
		int zahl2;
		

		// Display menu graphics
		System.out.println("====================================");
		System.out.println("| Was soll mit den Zahlen Geschen? |");
		System.out.println("=====================================");
		System.out.println("| Options:                          |");
		System.out.println("|        1. Addieren                |");
		System.out.println("|        2. Subtrahieren            |");
		System.out.println("|        3. Multiplizieren          |");
		System.out.println("|        4. Dividieren              |");
		System.out.println("|        5. Exit                    |");
		System.out.println("=====================================");
		System.out.print(" Select option: ");
		swValue = myScanner.next().charAt(0);
		
		if (swValue==5) {
			System.out.println("Good bye!");
			System.exit(0);
		}
		
		//Input
		System.out.println("Zahl eins: ");
		zahl1 = myScanner.nextInt();
		System.out.println("Zahl zwei: ");
		zahl2 = myScanner.nextInt();

		// Switch construct + output
		switch (swValue) {
		case '1':
			System.out.println(zahl1 +" + " + zahl2 +" = " + ts.add(zahl1, zahl2));
			break;
		case '2':
			System.out.println(zahl1 + " - " + zahl2 + " = " + ts.sub(zahl1, zahl2));
			break;
		case '3':
			System.out.println(zahl1 + " * " + zahl2 + " = " + ts.mul(zahl1, zahl2));
			break;
		case '4':
			System.out.println(zahl1 + " / " + zahl2 + " = " + ts.div(zahl1, zahl2));
			break;
		case '5':
			System.out.println("Good bye!");
			System.exit(0);
			break;
		  
		  
		default:
			System.out.println("Invalid selection");
			break; // This break is not really necessary
		}
	
		myScanner.close();
	}
	
}
