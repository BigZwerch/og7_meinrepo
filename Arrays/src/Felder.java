import java.sql.Array;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;

public class Felder {
	/**
	  *
	  * uebungsklasse zu Feldern
	  *
	  * @version 1.5 vom 21.09.2018
	  * @author Jörg
	  */

	  //unsere Zahlenliste zum Ausprobieren
	  private int[] zahlenliste = {5,8,4,3,9,1,2,7,6,0};
	  
	  //Konstruktor
	  public Felder(){}

	  //Methode die Sie implementieren sollen
	  //ersetzen Sie den Befehl return 0 durch return ihre_Variable
	  
	  //die Methode soll die gr��te Zahl der Liste zur�ckgeben
	  public int maxElement() {
	       int x = zahlenliste[0];
	       for (int i = 0; i < zahlenliste.length; i++) {
			if (zahlenliste[i]>x) {
				x=zahlenliste[i];
			}
	       return x;
	       }
	  }

	  //die Methode soll die kleinste Zahl der Liste zur�ckgeben
	  public int minElement(){
		  int x = zahlenliste[0];
	       for (int i = 0; i < zahlenliste.length; i++) {
			if (zahlenliste[i]<x) {
				x=zahlenliste[i];
			}
	       return x;
	  }
	  }
	  
	  //die Methode soll den abgerundeten Durchschnitt aller Zahlen zur�ckgeben
	  public int durchschnitt(){
		  int x=1;
	    for (int i = 0; i < zahlenliste.length; i++) {
	    	x=x*zahlenliste[i];
		}
	    x=x/zahlenliste.length;
	    return x;
	  }

	  //die Methode soll die Anzahl der Elemente zur�ckgeben
	  //der Befehl zahlenliste.length; k�nnte hierbei hilfreich sein
	  public int anzahlElemente(){
		  int x=zahlenliste.length;
		  return x;
	  }

	  //die Methode soll die Liste ausgeben
	  @Override
	public String toString(){
		 return zahlenliste.toString();
	  }

	  //die Methode soll einen booleschen Wert zur�ckgeben, ob der Parameter in
	  //dem Feld vorhanden ist
	  public boolean istElement(int zahl){
	    return false;
	  }
	  
	  //die Methode soll das erste Vorkommen der
	  //als Parameter übergebenen  Zahl liefern oder -1 bei nicht vorhanden
	  public int getErstePosition(int zahl){
	    for (int i = 0; i < zahlenliste.length; i++) {
			if (zahlenliste[i]==9) {
				return i;
			}
		}
		  return 0;
	  }
	  
	  //die Methode soll die Liste aufsteigend sortieren
	  //googlen sie mal nach    ;)
	  public void sortiere(){

	  }

			public static void main(String[] args) {
				  Felder testenMeinerLoesung = new Felder();
				  
			    System.out.println(testenMeinerLoesung.maxElement());
			    System.out.println(testenMeinerLoesung.minElement());
			    System.out.println(testenMeinerLoesung.durchschnitt());
			    System.out.println(testenMeinerLoesung.anzahlElemente());
			    System.out.println(testenMeinerLoesung.toString());
			    System.out.println(testenMeinerLoesung.istElement(9));
			    System.out.println(testenMeinerLoesung.getErstePosition(5));
			    testenMeinerLoesung.sortiere();
			    System.out.println(testenMeinerLoesung.toString());
			  }
	}