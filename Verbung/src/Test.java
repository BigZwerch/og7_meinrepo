public class Test {

	public static void main(String[] args) {
		Person p1 = new Person("Perter", 9132, false);
		Spieler s1 = new Spieler("Cewin", 1314, true, 25, "Ersatz Bank");
		Trainer t1 = new Trainer("Jogy Loef", 333, true, 'C', 500);
		Schiedsrichter e1 = new Schiedsrichter("Ulf", 7432, false, "Die Kikers 9000", 13);
		Mannschaftsleiter m1 = new Mannschaftsleiter("Julia", 4563211, false, 5, "Tor", "Overwatch", true);
		
		//output
		System.out.println("Person: "+p1.toString()+
				"\nSpieler: "+s1.toString() + 
				"\nTrainer: "+t1.toString()+ 
				"\nSchiedsrichter: "+e1.toString()+ 
				"\nMannschaftsleiter: "+m1.toString());
	}

}
