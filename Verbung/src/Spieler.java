public class Spieler extends Person{
	int trikoNr;
	String spielPos;
	
	@Override
	public String toString() {
		return "Spieler [name="+name +", teleNr="+teleNr+ ",bezahlt="+bezahlt + ", trikoNr=" + trikoNr + ", spielPos=" + spielPos + "]";
	}
	public Spieler(String name, int teleNr, boolean bezahlt, int trikoNr, String spielPos) {
		super(name, teleNr, bezahlt);
		this.trikoNr = trikoNr;
		this.spielPos = spielPos;
	}
	public int getTrikoNr() {
		return trikoNr;
	}
	public void setTrikoNr(int trikoNr) {
		this.trikoNr = trikoNr;
	}
	public String getSpielPos() {
		return spielPos;
	}
	public void setSpielPos(String spielPos) {
		this.spielPos = spielPos;
	}
}
