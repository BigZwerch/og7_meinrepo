public class Trainer extends Person {
	char lizensKlasse;
	int gehalt;
	
	public Trainer(String name, int teleNr, boolean bezahlt, char lizensKlasse, int gehalt) {
		super(name, teleNr, bezahlt);
		this.lizensKlasse = lizensKlasse;
		this.gehalt = gehalt;
	}
	
	@Override
	public String toString() {
		return "Trainer [name="+name +", teleNr="+teleNr+ ", bezahlt="+bezahlt + ", lizensKlasse=" + lizensKlasse + ", gehalt=" + gehalt + "]";
	}

	public char getLizensKlasse() {
		return lizensKlasse;
	}
	public void setLizensKlasse(char lizensKlasse) {
		this.lizensKlasse = lizensKlasse;
	}
	public int getGehalt() {
		return gehalt;
	}
	public void setGehalt(int gehalt) {
		this.gehalt = gehalt;
	}
}
