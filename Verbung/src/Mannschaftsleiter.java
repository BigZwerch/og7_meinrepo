public class Mannschaftsleiter extends Spieler{
	@Override
	public String toString() {
		return "Mannschaftsleiter [name="+name +", teleNr="+teleNr+", bezahlt="+bezahlt + ", trikoNr=" + trikoNr + ", spielPos=" + spielPos + ", nameMannschaft=" + nameMannschaft + ", engagement=" + engagement + "]";
	}

	String nameMannschaft;
	boolean engagement;
	
	public Mannschaftsleiter(String name, int teleNr, boolean bezahlt, int trikoNr, String spielPos,
			String nameMannschaft, boolean engagement) {
		super(name, teleNr, bezahlt, trikoNr, spielPos);
		this.nameMannschaft = nameMannschaft;
		this.engagement = engagement;
	}

	public String getNameMannschaft() {
		return nameMannschaft;
	}

	public void setNameMannschaft(String nameMannschaft) {
		this.nameMannschaft = nameMannschaft;
	}

	public boolean isEngagement() {
		return engagement;
	}

	public void setEngagement(boolean engagement) {
		this.engagement = engagement;
	}
}
