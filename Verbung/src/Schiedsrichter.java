public class Schiedsrichter extends Person{
	@Override
	public String toString() {
		return "Schiedsrichter [name="+name +", teleNr="+teleNr+ ", bezahlt="+bezahlt + ", nameMannschaft=" + nameMannschaft + ", gefiffenSpiele=" + gefiffenSpiele + "]";
	}
	String nameMannschaft;
	int gefiffenSpiele;
	
	public Schiedsrichter(String name, int teleNr, boolean bezahlt, String nameMannschaft, int gefiffenSpiele) {
		super(name, teleNr, bezahlt);
		this.nameMannschaft = nameMannschaft;
		this.gefiffenSpiele = gefiffenSpiele;
	}
	public String getNameMannschaft() {
		return nameMannschaft;
	}
	public void setNameMannschaft(String nameMannschaft) {
		this.nameMannschaft = nameMannschaft;
	}
	public int getGefiffenSpiele() {
		return gefiffenSpiele;
	}
	public void setGefiffenSpiele(int gefiffenSpiele) {
		this.gefiffenSpiele = gefiffenSpiele;
	}
}
