public class Person {
	String name;
	int teleNr;
	boolean bezahlt;
	
	public Person(String name, int teleNr, boolean bezahlt) {
		super();
		this.name = name;
		this.teleNr = teleNr;
		this.bezahlt = bezahlt;
	}
	
	@Override
	public String toString() {
		return "Person [name=" + name + ", teleNr=" + teleNr + ", bezahlt=" + bezahlt + "]";
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getTeleNr() {
		return teleNr;
	}
	public void setTeleNr(int teleNr) {
		this.teleNr = teleNr;
	}
	public boolean isBezahlt() {
		return bezahlt;
	}
	public void setBezahlt(boolean bezahlt) {
		this.bezahlt = bezahlt;
	}
}
