/**
 * ermoeglicht die Erstellung von Piloten und die Interacktion mit diesen
 * 
 * @author J�rg Wolter
 * @version 13.09.2018
 */
public class Pilot extends StarSim {

	// Attribute
	String name;
	String grad;
	// Methoden
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGrad() {
		return grad;
	}
	public void setGrad(String grad) {
		this.grad = grad;
	}

}
