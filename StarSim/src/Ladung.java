/**
 * Write a description of class Ladung here.
 * ermoeglicht die Erstellung von Ladung und die Interackion mit dieser
 * @author J�rg Wolter
 * @version 13.09.2018
 */
public class Ladung {

	// Attribute
	double posX;
	double posY;
	int masse;
	String typ;
	
	// Methoden

	public double getPosX() {
		return posX;
	}

	public void setPosX(double posX) {
		this.posX = posX;
	}

	public double getPosY() {
		return posY;
	}

	public void setPosY(double posY) {
		this.posY = posY;
	}

	public int getMasse() {
		return masse;
	}

	public void setMasse(int masse) {
		this.masse = masse;
	}

	public String getTyp() {
		return typ;
	}

	public void setTyp(String typ) {
		this.typ = typ;
	}

	// Darstellung
	public static char[][] getDarstellung() {
		char[][] ladungShape = { { '/', 'X', '\\' }, { '|', 'X', '|' }, { '\\', 'X', '/' } };
		return ladungShape;
	}
}