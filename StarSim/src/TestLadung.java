public class TestLadung extends StarSim {

	public static void main(String[] args) {
		
		int masse = 122;
		String typ = "Test-Ladung (gepunktet)";
		
		Ladung meineLadung = new Ladung();
		meineLadung.setTyp(typ);
		meineLadung.setMasse(masse);
		
		if (meineLadung.getTyp().equals(typ))
			System.out.println("Implementierung 'Typ'  korrekt!");
		
		if (meineLadung.getMasse() == masse)
			System.out.println("Implementierung 'Masse'  korrekt!");
	}

}
