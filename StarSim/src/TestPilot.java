
public class TestPilot extends StarSim{

	public static void main(String[] args) {
		String grad = "Testpilot";
		String name = "Max Musterpilot";
		
		Pilot meinPilot = new Pilot();
		meinPilot.setGrad(grad);
		meinPilot.setName(name);
				
		if (meinPilot.getGrad().equals(grad))
			System.out.println("Implementierung 'Grad'  korrekt!");
		
		if (meinPilot.getName().equals(name))
			System.out.println("Implementierung 'Name'  korrekt!");

}
	
}

