/**
 * New Raumschiff and visible show of it
 * 
 * @author J�rg Wolter
 * @version 07.09.2018
 */
public class Raumschiff {

	// Attribute
	String typ;
	String antrieb;
	int maxLadekapazitaet;
	int winkel;
	double posX;
	double posY;
	// Methoden

	public String getTyp() {
		return typ;
	}

	public void setTyp(String typ) {
		this.typ = typ;
	}

	public String getAntrieb() {
		return antrieb;
	}

	public void setAntrieb(String antrieb) {
		this.antrieb = antrieb;
	}

	public int getMaxLadekapazitaet() {
		return maxLadekapazitaet;
	}

	public void setMaxLadekapazitaet(int maxLadekapazitaet) {
		this.maxLadekapazitaet = maxLadekapazitaet;
	}

	public int getWinkel() {
		return winkel;
	}

	public void setWinkel(int winkel) {
		this.winkel = winkel;
	}

	public double getPosX() {
		return posX;
	}

	public void setPosX(double posX2) {
		this.posX = posX2;
	}

	public double getPosY() {
		return posY;
	}

	public void setPosY(double posY) {
		this.posY = posY;
	}

	// Darstellung
	public static char[][] getDarstellung() {
		char[][] raumschiffShape = { 
				{'\0', '\0','_', '\0', '\0'},
				{'\0', '/', 'X', '\\', '\0'},
				{'\0', '{', 'X', '}', '\0'},
				{'\0', '{', 'X', '}', '\0'},
				{'/', '_', '_','_', '\\'},				
		};
		return raumschiffShape;
	}
}
