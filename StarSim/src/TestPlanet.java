

public class TestPlanet extends StarSim {

	public static void main(String[] args) {
		int anzahlHafen = 3;
		String name = "Max Musterpilot";
		
		Planet meinPlanet = new Planet();
		meinPlanet.setAnzahlHafen(anzahlHafen);
		meinPlanet.setName(name);
		
		if (meinPlanet.getAnzahlHafen() == anzahlHafen)
			System.out.println("Implementierung 'Hafen' korrekt!");
		
		if (meinPlanet.getName().equals(name))
			System.out.println("Implementierung 'Name'  korrekt!");
	}

}
