package collection_test;
import java.util.*;

import ms3.Buch;

public class CollectionTest {
	
	static void menue() {
		System.out.println("\n ***** Buch-Verwaltung *******");
		System.out.println(" 1) eintragen ");
		System.out.println(" 2) finden ");
		System.out.println(" 3) loeschen");
		System.out.println(" 4) Die groe�te ISBN");
		System.out.println(" 5) zeigen");
		System.out.println(" 9) Beenden");
		System.out.println(" *****************************");
		System.out.print(" Bitte die Auswahl treffen: ");
	} // menue

	public static void main(String[] args) {

		List<Buch> buchliste = new LinkedList<Buch>();
        Scanner myScanner = new Scanner(System.in);

		char wahl;
		String eintrag;
		String author;
		String vertrieb;
		String isbn;
		Buch b = new Buch();
		int index;
		do {
			menue();
			wahl = myScanner.next().charAt(0);
			switch (wahl) {
			case '1':	
				System.out.println("\n Autor eingeben: ");
				author=myScanner.next();
				b.setAuthor(author);
				System.out.println("\n Vertieb eingeben: ");
				vertrieb=myScanner.next();
				System.out.println("\n ISBN eingeben: ");
				isbn=myScanner.next();
                buchliste.add(new Buch(author, vertrieb, isbn));
				break;
			case '2':
				System.out.println("\n Zu suchende Isbn eingeben: ");
				isbn=myScanner.next();
				CollectionTest.findeBuch(buchliste, isbn);
				break;
			case '3':
				System.out.println("\n Zu l�schende Isbn eingeben: ");
				isbn=myScanner.next();
				CollectionTest.loescheBuch(buchliste, isbn);
				System.err.println("Nicht gefunden");
				break;
			case '4':
				CollectionTest.ermitteleGroessteISBN(buchliste);
				break;		
			case '5':
				System.out.println(buchliste.toString());
				break;
			case '9':
				System.exit(0);
				break;
			default:
				menue();
				wahl = myScanner.next().charAt(0);
			} // switch

		} while (wahl != 9);
	}// main
	public static Buch findeBuch(List<Buch> buchliste, String isbn) {
		for (int i = 0; i < buchliste.size(); i++) {
			if(buchliste.get(i).getIsbn().equals(isbn)) {
				System.out.println("Gefunden an Stelle: " + i);
				System.out.println("Ihr Buch: " + buchliste.get(i));
			}
		}
		return null;
	}

	public static boolean loescheBuch(List<Buch> buchliste, String isbn) {
		for (int i = 0; i < buchliste.size(); i++) {
			if(buchliste.get(i).getIsbn().equals(isbn)) {
				System.out.println("Gefunden an Stelle: " + i);
				System.out.println("Ihr Buch: " + buchliste.remove(i));
			}
		}
		return false;
	}

	public static String ermitteleGroessteISBN(List<Buch> buchliste) {
		
		
		
		
		return "";
	}

}