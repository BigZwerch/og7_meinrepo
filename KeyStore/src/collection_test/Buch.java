package collection_test;

public class Buch implements Comparable<Buch> {
	String author;
	String vertrieb;
	String isbn;
	
	public Buch(String author, String vertrieb, String isbn) {
		super();
		this.author = author;
		this.vertrieb = vertrieb;
		this.isbn = isbn;
	}
	public Buch() {
		super();
	}
	
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getVertrieb() {
		return vertrieb;
	}
	public void setVertrieb(String vertrieb) {
		this.vertrieb = vertrieb;
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((author == null) ? 0 : author.hashCode());
		result = prime * result + ((isbn == null) ? 0 : isbn.hashCode());
		result = prime * result + ((vertrieb == null) ? 0 : vertrieb.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Buch other = (Buch) obj;
		if (author == null) {
			if (other.author != null)
				return false;
		} else if (!author.equals(other.author))
			return false;
		if (isbn == null) {
			if (other.isbn != null)
				return false;
		} else if (!isbn.equals(other.isbn))
			return false;
		if (vertrieb == null) {
			if (other.vertrieb != null)
				return false;
		} else if (!vertrieb.equals(other.vertrieb))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Buch [author= " + author + ", vertrieb= " + vertrieb + ", isbn= " + isbn + "]";
	}
	@Override
	public int compareTo(Buch buch) {
		return this.getIsbn().compareTo(buch.getIsbn());
	}	
}

