package vorbereitungMS3;
import java.util.ArrayList;
import java.util.Scanner;

public class StringArrayTest extends ArrayList<String> {
	
	static void menue() {
		System.out.println("\n ***** String-Verwaltung *******");
		System.out.println(" 1) eintragen ");
		System.out.println(" 2) finden ");
		System.out.println(" 3) zeigen ");
		System.out.println(" 4) Sortieren ");
		System.out.println(" 5) Beenden ");
		System.out.println(" *******************************");
		System.out.print(" Bitte die Auswahl treffen: ");
	} // menue
	
	public static void bubblesort(String[] zusortieren) {
		String tmps;
		for(int i=1; i<zusortieren.length; i++) {
			for(int j=0; j<zusortieren.length-i; j++) {
				if(zusortieren[i].compareToIgnoreCase(zusortieren[i+1])>0) {
					tmps=zusortieren[j];
					zusortieren[j]=zusortieren[j+1];
					zusortieren[j+1]=tmps;
				}
				
			}
		}
	}
	
	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		String e = new String();
		ArrayList<String> s = new ArrayList<String>();
		int index;
		char wahl;
		do {
			menue();
			wahl = myScanner.next().charAt(0);
			switch (wahl) {
			case '1':
				System.out.print("\nString: ");
				e = myScanner.next();
				if (s.add(e))
					System.out.println("String wurde eingetragen\n");
				else
					System.out.println("Array voll \n");
				break;
			case '2':
				System.out.print("\nGesuchter String: ");
				e = myScanner.next();
				index = s.indexOf(e);
				if (index >= 0)
					System.out.println("Eintrag wurde an " + index + ". Stelle gefunden\n");
				else
					System.out.println("Eintrag nicht gefunden\n");
				break;
			case '3':
				System.out.println("\n  "+s);
				break;
			case '4':
				String tmps;
				String[] zusortieren ;
				for(int i=1; i<zusortieren.length; i++) {
					for(int j=0; j<zusortieren.length-i; j++) {
						if(zusortieren[i].compareToIgnoreCase(zusortieren[i+1])>0) {
							tmps=zusortieren[j];
							zusortieren[j]=zusortieren[j+1];
							zusortieren[j+1]=tmps;
						}
						
					}
				}
				break;
			case '5':
				System.exit(0);
				break;
			default:
				menue();
				wahl = myScanner.next().charAt(0);
			} // switch

		} while (wahl != 5);
		
	}

}
