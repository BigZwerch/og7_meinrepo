package keyStore01;

public class KeyStore01 {
	private String[] Keys;
	
	public KeyStore01() {
		this.Keys = new String[100];
	}
	public KeyStore01(int length) {
		this.Keys = new String[length];
	}
	
	public boolean add(String eintrag) {
		for (int i = 0; i < Keys.length;i++) {
			if (this.Keys[i]==null) {
				this.Keys[i]=eintrag;
				return true;
			}
		}
		return false;
	}

	public String toString() {
		String s= "";
		for (int i = 0; i < Keys.length; i++) {
			if (this.Keys[i]!=null) {
				s+=this.Keys[i]+"\n";	
			}
		}
		return s;
	}
	
	public int indexOf(String eintrag) {
	for(int i = 0; i<Keys.length;i++) {
		if(Keys[i]==eintrag) {
			return i;
		}
	}
		return -1;
	}

	public void remove(int index) {
		this.Keys[index] = null;
		for (; index < this.Keys.length-1; index++) {
			this.Keys[index] = this.Keys[index+1];	
		}
		this.Keys[this.Keys.length-1]=null;
	}
	
}
