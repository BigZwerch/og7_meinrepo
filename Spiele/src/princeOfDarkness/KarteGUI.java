package princeOfDarkness;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridLayout;
import javax.swing.JTextField;
import javax.swing.JButton;

public class KarteGUI extends JFrame {

	private JPanel contentPane;
	private JTextField txtHeldname;
	private JTextField txtTyp;
	private JTextField txtBild;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					KarteGUI frame = new KarteGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public KarteGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(0, 2, 0, 0));
		
		txtHeldname = new JTextField();
		txtHeldname.setEditable(false);
		txtHeldname.setText("Heldname");
		contentPane.add(txtHeldname);
		txtHeldname.setColumns(10);
		
		txtTyp = new JTextField();
		txtTyp.setEditable(false);
		txtTyp.setText("Typ");
		contentPane.add(txtTyp);
		txtTyp.setColumns(10);
		
		txtBild = new JTextField();
		txtBild.setEditable(false);
		txtBild.setText("Bild");
		contentPane.add(txtBild);
		txtBild.setColumns(10);
	}

}
