package BorderL;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class BorderL extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BorderL frame = new BorderL();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public BorderL() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		int i = 1;
		long MILLIS = 20000;
		
		JButton north = new JButton("North");
		north.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				change(north);
			}
			private void change(JButton btn){
				if(btn.getText().equals("North") && i==1) {
					btn.setText("Hier");
					try {
						int MAX = 2000;
						for (int j = 0; j < MAX; j++) 
							this.wait(MILLIS);
							
						
						btn.setText("Loser");
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}else {
					btn.setText(" ");
				}
				
			}
		});
		contentPane.add(north, BorderLayout.NORTH);
		
		
		JButton West = new JButton("West");
		West.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		contentPane.add(West, BorderLayout.WEST);
		
		JButton South = new JButton("South");
		South.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		contentPane.add(South, BorderLayout.SOUTH);
		
		JButton East = new JButton("East");
		East.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		contentPane.add(East, BorderLayout.EAST);
		
		JButton Center = new JButton("Center");
		Center.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		contentPane.add(Center, BorderLayout.CENTER);
		
	}

}
