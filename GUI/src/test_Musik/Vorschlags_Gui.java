package test_Musik;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.sun.prism.Image;

import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.awt.event.ActionEvent;

public class Vorschlags_Gui extends JFrame {

	private JPanel contentPane;
	private JTextField txt_Name;
	private JTextField txt_Vorname;
	private JTextField txt_Passwort;
	private JTextField txt_Interpret;
	private JTextField txt_Genre;
	private JTextField txt_Tietel;
	private String interpret;
	private String titele;
	private String genre;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Vorschlags_Gui frame = new Vorschlags_Gui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Vorschlags_Gui() {
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(4, 2, 0, 0));

		JLabel lbl_Interpret = new JLabel("Interpret");
		contentPane.add(lbl_Interpret);
		
		txt_Interpret = new JTextField();
		txt_Interpret.setText("");
		contentPane.add(txt_Interpret);
		txt_Interpret.setColumns(10);
		
		JLabel lbl_Tietel = new JLabel("Tietel");
		contentPane.add(lbl_Tietel);
		
		txt_Tietel = new JTextField();
		txt_Tietel.setText("");
		contentPane.add(txt_Tietel);
		txt_Tietel.setColumns(10);
		
		JLabel lbl_Genre = new JLabel("Genre");
		contentPane.add(lbl_Genre);
		
		txt_Genre = new JTextField();
		txt_Genre.setText("");
		contentPane.add(txt_Genre);
		txt_Genre.setColumns(10);
		
		
		JButton btn_loeschen = new JButton("L�schen");
		BufferedImage buttonIcon_Loeschen;
		try {
			buttonIcon_Loeschen = ImageIO.read(new File("./image/Kreutz.png"));
			btn_loeschen = new JButton(new ImageIcon(buttonIcon_Loeschen));
			btn_loeschen.setText("L�schen");
			btn_loeschen.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					txt_Interpret.setText("");
					txt_Tietel.setText("");
					txt_Genre.setText("");
				}
			});
			contentPane.add(btn_loeschen);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		JButton btn_voten = new JButton("Voten");
		BufferedImage buttonIcon_Voute;
		try {
			buttonIcon_Voute = ImageIO.read(new File("./image/gruener-haken.png"));
			btn_voten = new JButton(new ImageIcon(buttonIcon_Voute));
			btn_voten.setText("Eintragen");
			btn_voten.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if(txt_Interpret.getText().equals("")|txt_Tietel.getText().equals("")|txt_Genre.getText().equals("")) {
						if(txt_Interpret.getText().equals("")) {
							JOptionPane.showMessageDialog(null, "Interpret darf nicht leer sein!",
									"ERROR",
								    JOptionPane.ERROR_MESSAGE);
						}
						if(txt_Tietel.getText().equals("")) {
							JOptionPane.showMessageDialog(null, "Titel darf nicht leer sein!",
									"ERROR",
								    JOptionPane.ERROR_MESSAGE);
						}
						if(txt_Genre.getText().equals("")) {
							JOptionPane.showMessageDialog(null, "Genre darf nicht leer sein!",
									"ERROR",
								    JOptionPane.ERROR_MESSAGE);
						}
					}else {
						
						txt_Interpret.setText("");
						txt_Tietel.setText("");
						txt_Genre.setText("");
					}
					
					//ctrl.eintragen(gast, lied);
					
				}
			});
			contentPane.add(btn_voten);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

}
