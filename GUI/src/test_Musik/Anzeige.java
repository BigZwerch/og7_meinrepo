package test_Musik;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.GridLayout;
import java.util.LinkedList;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class Anzeige extends JFrame {


	private JPanel contentPane;
	private String name;
	private String vorname;
	private String interpret;
	private String titele;
	private String genre;
	LinkedList lieder_list = new LinkedList(); 
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Anzeige frame = new Anzeige();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Anzeige() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		// Die Daten f�r die JTable
	      Object[][] data = new Object[][]{ 
	    	  {},
	    	  {}
	      };//
	      
	      // Die Titel der Spalten
	      String[] spaltennamen = {"Anzahl Votes", "Bandname", "Titelname", "Genre"};
		JTable table = new JTable( data, spaltennamen);
		JScrollPane panel_Lieder = new JScrollPane(table);
		contentPane.add(panel_Lieder, BorderLayout.CENTER);	
	}
}
