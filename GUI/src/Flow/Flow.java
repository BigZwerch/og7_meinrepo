package Flow;

import java.awt.Color;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JToolBar;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

public class Flow extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					Flow frame = new Flow();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Flow() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
	
		
		textField = new JTextField();
		textField.setEditable(false);
		contentPane.add(textField);
		textField.setColumns(10);
	
		
		int wert = 1117;
		int zufall = (int) (Math.random() * wert);
		long start = System.currentTimeMillis();
		textField.setText(zufall+"");
		
		System.out.println(zufall);
		for (int i = 0; i < wert; i++) {
			JRadioButton btnNewButton = new JRadioButton(""+i);
			if (i == zufall) {
				btnNewButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent arg0) {
						change(btnNewButton);
					}

					public void change(JRadioButton b) {
						b.setForeground(Color.GREEN);
						System.out.println("Du hast " + (System.currentTimeMillis()-start) + " milliSekunden gebraucht!");
						//System.exit(0);
						
					}
				});
			} else {
				btnNewButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent arg0) {
						change(btnNewButton);
					}

					public void change(JRadioButton b) {
						b.setForeground(Color.RED);
					}
				});
			}
			contentPane.add(btnNewButton);
		}
	}

}
