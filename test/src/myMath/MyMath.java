package myMath;

public class MyMath {
	public int fakultaet(int zahl) {
		int ausgabe = 0;
		for(int i = zahl; i > 0; i--) {
			ausgabe=ausgabe*i;
		}
		return ausgabe;
	}
	
	public int fibonacci(int zahl) {
		int ausgabe = 0;
		for(int i = 0; i< zahl; i++) {
			ausgabe= ausgabe+i;
		}
		return ausgabe;
	}
}
