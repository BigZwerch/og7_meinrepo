package zootykoonAddon;

public class ZootykoonAddon {
	int id;
	String bezeichnung;
	double preis;
	int aktMenge;
	int maxMenge;
	
	public ZootykoonAddon() {
		super();
	}
	
	public ZootykoonAddon(int id, String bezeichnung, double preis, int aktMenge, int maxMenge) {
		super();
		this.id = id;
		this.bezeichnung = bezeichnung;
		this.preis = preis;
		this.aktMenge = aktMenge;
		this.maxMenge = maxMenge;
	}
	
	@Override
	public String toString() {
		return "ZootykoonAddon [id=" + id + ", bezeichnung=" + bezeichnung + ", preis=" + preis + "�, aktMenge="
				+ aktMenge + ", maxMenge=" + maxMenge + "]";
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getBezeichnung() {
		return bezeichnung;
	}
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}
	public double getPreis() {
		return preis;
	}
	public void setPreis(double preis) {
		this.preis = preis;
	}
	public int getAktMenge() {
		return aktMenge;
	}
	public void setAktMenge(int aktMenge) {
		this.aktMenge = aktMenge;
	}
	public int getMaxMenge() {
		return maxMenge;
	}
	public void setMaxMenge(int maxMenge) {
		this.maxMenge = maxMenge;
	}
}
