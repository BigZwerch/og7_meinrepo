import java.util.Scanner;

public class Primzahl {
	long pruefen;
	long zahl;
	Stopuhr t  = new Stopuhr();
	Scanner scan = new Scanner(System.in);
	
	public void pruefen() {
		long zahl = scan.nextLong();
		t.nstarte();
		if(zahl<=1) {
	        System.out.println("Die Zahl "+zahl+" ist nicht groesser 1, also keine Primzahl!" );
	        t.nstope();
			System.out.println("Zeit: "+t.nlies()+" nano sec.");
	        return;
	    }
	    if (zahl%2==0) {//ist Zahl grade?
	      System.out.println("Die Zahl " +zahl+" ist gerade!" );
	      t.mstope();
			System.out.println("Zeit: "+t.nlies()+" nano sec.");
	      return;
	    }
	    System.out.println("Die Zahl 2 teilt nicht "+zahl );
	    int i = 3;
	    while( i*i < zahl & zahl % i !=0) {
	        //System.out.println("Die Zahl "+i+" teilt nicht "+zahl );
	      i=i+2;
	    }
	    // Welch Bedingung fuehrte zum Abbruch?
	     if( i*i>zahl) { // Keinen Teiler von zahl gefunden
	       System.out.println("Die "+zahl+" ist eine Primzahl!" );
	       t.nstope();
			System.out.println("Zeit: "+t.nlies()+" nano sec.");
	     } else { // i ist Teiler
	       System.out.println("Die Zahl "+i+" teilt "+zahl+"!" );
	       t.nstope();
			System.out.println("Zeit: "+t.nlies()+" nano sec.");
	    }
	}
	}