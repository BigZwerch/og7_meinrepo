package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;

import com.mysql.jdbc.Statement;

public class MySQLDatabase {
	
	private final String driver = "com.mysql.jdbc.Driver";
	private final String url = "jdbc:mysql://localhost/rechtecke?";
	private final String user = "root";
	private final String password = "";
	
	public void rechteckEintragen(Rechteck r) {
		try{
			// JDBC-Treiber laden
				Class.forName(driver);
				// Verbindung aufbauen
				Connection con;
				con = DriverManager.getConnection(url, user, password);
					
				String sql ="INSERT INTO T_Rechtecke (x,y,breite,hoehe) VALUES" + 
						"(?,?,?,?);";
				PreparedStatement ps = con.prepareStatement(sql);
				ps.setInt(1, r.getX());
				ps.setInt(2, r.getY());
				ps.setInt(3, r.getBreite());
				ps.setInt(4, r.getHoehe());
				ps.executeUpdate();		
						
						
						
				con.close();
			}catch (Exception e) { // Fehler abfangen
				e.printStackTrace();// Fehlermeldung ausgeben
			}
	
	}
	
	public List<Rechteck> getAlleRechtecke(){
		List rechtecke = new LinkedList<Rechteck>();
		try{
			// JDBC-Treiber laden
			Class.forName(driver);
			// Verbindung aufbauen
			Connection con;
			con = DriverManager.getConnection(url, user, password);
				
			String sql ="SELECT * FROM T_Rechtecke;";
			Statement s = (Statement) con.createStatement();
			ResultSet rs = s.executeQuery(sql);
			//solange n�chstes Objekt in rs vorhanden
			while(rs.next()) {
				int x = rs.getInt(2);
				int y = rs.getInt("Y");
				int breite = rs.getInt("breite");
				int hoehe = rs.getInt("hoehe");
				rechtecke.add(new Rechteck(x,y,breite,hoehe));
			}
			
			con.close();
			}catch (Exception e) { // Fehler abfangen
				e.printStackTrace();// Fehlermeldung ausgeben
			}
		return rechtecke;
	}
}