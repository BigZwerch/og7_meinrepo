package controller;

import java.util.LinkedList;
import java.util.List;

import model.MySQLDatabase;
import model.Rechteck;
import view.RechteckNeueGui;

public class BunteRechteckeController {
	
	//Attribute
		private List<Rechteck> rechtecke;	
		private MySQLDatabase database;

		//Konstruktoren
		public BunteRechteckeController() {
			super();
			//rechtecke = new LinkedList<Rechteck>();
			this.database = new MySQLDatabase();
			rechtecke = this.database.getAlleRechtecke();
		}

		public BunteRechteckeController(List<Rechteck> rechtecke) {
			super();
			this.rechtecke = rechtecke;
		}
	//Methoden
		//einfuegen
		public void add(Rechteck rechteck){
			rechtecke.add(rechteck);
			database.rechteckEintragen(rechteck);
		}
		
		//reset
		public void reset() {
			rechtecke.clear();
		}
		
		//String
		@Override
		public String toString() {
			return "BunteRechteckeController [rechtecke=" + rechtecke + "]";
		}
	
	//getter
	public List<Rechteck> getRechtecke() {
			return rechtecke;
	}
	//setter
	public void setRechtecke(List<Rechteck> rechtecke) {
		this.rechtecke = rechtecke;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public void rechteckHinzufuegen() {
		new RechteckNeueGui(this);
		
	}

}
