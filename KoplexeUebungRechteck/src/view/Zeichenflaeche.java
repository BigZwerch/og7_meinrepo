package view;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;
import controller.BunteRechteckeController;

public class Zeichenflaeche extends JPanel{
	
	private BunteRechteckeController brc;
	
	public Zeichenflaeche(BunteRechteckeController brc){
		this.brc = brc;
	}
	
	@Override
	public void paintComponent(Graphics g) {
		g.setColor(Color.BLACK);
		//g.drawRect(0, 0, 50, 50);
		for (int i = 0; i < brc.getRechtecke().size(); i++) {
			int breite = brc.getRechtecke().get(i).getBreite();
			int hoehe  = brc.getRechtecke().get(i).getHoehe();
			int x = brc.getRechtecke().get(i).getX();
			int y = brc.getRechtecke().get(i).getY();
			g.drawRect(x,y,breite,hoehe);
		}
	}
}
