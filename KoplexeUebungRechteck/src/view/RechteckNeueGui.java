package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.BunteRechteckeController;
import model.Rechteck;

import java.awt.GridLayout;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;

public class RechteckNeueGui extends JFrame {

	private JPanel contentPane;
	private JTextField textX;
	private JTextField textY;
	private JTextField textHoehe;
	private JTextField textBreite;
	private JButton btnSpeichern;
	private BunteRechteckeController brc;
	private JLabel lblX;
	private JLabel lblY;
	private JLabel lblHhe;
	private JLabel lblBreite;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RechteckNeueGui frame = new RechteckNeueGui(new BunteRechteckeController());
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @param bunteRechteckeController 
	 */
	public RechteckNeueGui(BunteRechteckeController bunteRechteckeController) {
		this.brc = bunteRechteckeController;
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(9, 1, 0, 0));
		
		lblX = new JLabel("X");
		contentPane.add(lblX);
		
		textX = new JTextField();
		contentPane.add(textX);
		textX.setColumns(10);
		
		lblY = new JLabel("Y");
		contentPane.add(lblY);
		
		textY = new JTextField();
		contentPane.add(textY);
		textY.setColumns(10);
		
		lblHhe = new JLabel("Hoehe");
		contentPane.add(lblHhe);
		
		textHoehe = new JTextField();
		contentPane.add(textHoehe);
		textHoehe.setColumns(10);
		
		lblBreite = new JLabel("Breite");
		contentPane.add(lblBreite);
		
		textBreite = new JTextField();
		contentPane.add(textBreite);
		textBreite.setColumns(10);
		
		btnSpeichern = new JButton("Speichern");
		btnSpeichern.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				speichern();
			}
		});
		contentPane.add(btnSpeichern);
		setVisible(true);
	}

	protected void speichern() {
		int x = Integer.parseInt(textX.getText());
		int y = Integer.parseInt(textY.getText());
		int laenge = Integer.parseInt(textHoehe.getText());
		int breite = Integer.parseInt(textBreite.getText());
		Rechteck r = new Rechteck(x, y, laenge, breite);
		this.brc.add(r);
		
	}

}
