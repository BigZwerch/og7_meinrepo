package sortierer;

public class BubbleSort {	
	public static int[] bubblesort(int[] zusortieren) {
		long time = System.currentTimeMillis();	
		int temp;
		for(int i=1; i<zusortieren.length; i++) {
			for(int j=0; j<zusortieren.length-i; j++) {
				if(zusortieren[j]>zusortieren[j+1]) {
					temp=zusortieren[j];
					zusortieren[j]=zusortieren[j+1];
					zusortieren[j+1]=temp;
				}
				
			}
		}
		System.out.println(System.currentTimeMillis() - time + "ms");
		return zusortieren;
	}
	

	
	public static void main(String[] args) {
		
		int[] unsortiert={99,98,97,96,95,94,93,92,91,90,89,88,87,86,85,1001,84,83,82,81,80,70,60,50,40,30,20,19,18,17,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1};
		int[] sortiert=bubblesort(unsortiert);
		
		for (int i = 0; i<sortiert.length; i++) {
			System.out.print(sortiert[i] + ", ");
		}
	
	}
}
